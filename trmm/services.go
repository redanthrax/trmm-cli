// Package trmm integrates with the Tactical RMM API.
package trmm

type Services struct {
	Name        string `json:"name"`
	Status      string `json:"status"`
	DisplayName string `json:"display_name"`
	Binpath     string `json:"binpath"`
	Description string `json:"description"`
	Username    string `json:"username"`
	Pid         int    `json:"pid"`
	StartType   string `json:"start_type"`
	Autodelay   bool   `json:"autodelay"`
}
