// Package trmm integrates with the Tactical RMM API.
package trmm

// Logs represents all Logs endpoints.
type Logs struct {
	PendingActions        *[]LogsPendingActions
	PendingActionsDetails *[]LogsPendingActionsDetails
}

type LogsPendingActions struct {
	Id          int                       `json:"id" csv:"id"`
	Hostname    string                    `json:"hostname" csv:"hostname"`
	Client      string                    `json:"client" csv:"client"`
	Site        string                    `json:"site" csv:"site"`
	Due         string                    `json:"due" csv:"due"`
	Description string                    `json:"description" csv:"description"`
	EntryTime   string                    `json:"entry_time" csv:"entry_time"`
	ActionType  string                    `json:"action_type" csv:"action_type"`
	Status      string                    `json:"status" csv:"status"`
	Cancelable  bool                      `json:"cancelable" csv:"cancelable"`
	CeleryId    interface{}               `json:"celery_id" csv:"celery_id"`
	Details     LogsPendingActionsDetails `json:"details" csv:"details"`
	Agent       int                       `json:"agent" csv:"agent"`
}

type LogsPendingActionsDetails struct {
	Url       string `json:"url,omitempty" csv:"url,omitempty"`
	Inno      string `json:"inno,omitempty" csv:"inno,omitempty"`
	Version   string `json:"version,omitempty" csv:"version,omitempty"`
	Name      string `json:"name,omitempty" csv:"name,omitempty"`
	Output    string `json:"output,omitempty" csv:"output,omitempty"`
	Installed bool   `json:"installed,omitempty" csv:"installed,omitempty"`
}

// GetLogsPendingActions will get the LogsPendingActions from the TRMM API.
func (t *TRMM) GetLogsPendingActions() *TRMM {
	if t.err != nil {
		return t
	}

	t.Logs.PendingActions = t.setURL("logs/pendingactions").
		apiGet([]LogsPendingActions{}).
		response.Result().(*[]LogsPendingActions)
	return t
}
