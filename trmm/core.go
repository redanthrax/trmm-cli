// Package trmm integrates with the Tactical RMM API.
package trmm

import (
	"strings"
	"time"
)

// Core represents all Core endpoints.
type Core struct {
	CodeSign     *CoreCodeSign
	CustomFields *[]CoreCustomFields
	DashInfo     *CoreDashInfo
	KeyStore     *[]CoreKeyStore
	Settings     *CoreSettings
	Version      *CoreVersion
}

type CoreCodeSign struct {
	Token string `json:"token"`
}

type CoreCustomFields struct {
	Id                    int           `json:"id" csv:"id"`
	CreatedBy             string        `json:"created_by" csv:"created_by"`
	CreatedTime           string        `json:"created_time" csv:"created_time"`
	ModifiedBy            string        `json:"modified_by" csv:"modified_by"`
	ModifiedTime          string        `json:"modified_time" csv:"modified_time"`
	Order                 int           `json:"order" csv:"order"`
	Model                 string        `json:"model" csv:"model"`
	Type                  string        `json:"type" csv:"type"`
	Options               []interface{} `json:"options" csv:"options"`
	Name                  string        `json:"name" csv:"name"`
	Required              bool          `json:"required" csv:"required"`
	DefaultValueString    string        `json:"default_value_string" csv:"default_value_string"`
	DefaultValueBool      bool          `json:"default_value_bool" csv:"default_value_bool"`
	DefaultValuesMultiple []interface{} `json:"default_values_multiple" csv:"default_values_multiple"`
	HideInUi              bool          `json:"hide_in_ui" csv:"hide_in_ui"`
}

type CoreDashInfo struct {
	TrmmVersion              string      `json:"trmm_version" csv:"trmm_version"`
	LatestTrmmVer            string      `json:"latest_trmm_ver" csv:"latest_trmm_ver"`
	DarkMode                 bool        `json:"dark_mode" csv:"dark_mode"`
	ShowCommunityScripts     bool        `json:"show_community_scripts" csv:"show_community_scripts"`
	DblClickAction           string      `json:"dbl_click_action" csv:"dbl_click_action"`
	DefaultAgentTblTab       string      `json:"default_agent_tbl_tab" csv:"default_agent_tbl_tab"`
	UrlAction                interface{} `json:"url_action" csv:"url_action"`
	ClientTreeSort           string      `json:"client_tree_sort" csv:"client_tree_sort"`
	ClientTreeSplitter       int         `json:"client_tree_splitter" csv:"client_tree_splitter"`
	LoadingBarColor          string      `json:"loading_bar_color" csv:"loading_bar_color"`
	ClearSearchWhenSwitching bool        `json:"clear_search_when_switching" csv:"clear_search_when_switching"`
	Hosted                   bool        `json:"hosted" csv:"hosted"`
}

type CoreKeyStore struct {
	Id           int       `json:"id" csv:"id"`
	CreatedBy    string    `json:"created_by" csv:"created_by"`
	CreatedTime  time.Time `json:"created_time" csv:"created_time"`
	ModifiedBy   string    `json:"modified_by" csv:"modified_by"`
	ModifiedTime time.Time `json:"modified_time" csv:"modified_time"`
	Name         string    `json:"name" csv:"name"`
	Value        string    `json:"value" csv:"value"`
}

type CoreSettings struct {
	Id                      int           `json:"id" csv:"id"`
	AllTimezones            []string      `json:"all_timezones" csv:"all_timezones"`
	CreatedBy               interface{}   `json:"created_by" csv:"created_by"`
	CreatedTime             string        `json:"created_time" csv:"created_time"`
	ModifiedBy              interface{}   `json:"modified_by" csv:"modified_by"`
	ModifiedTime            string        `json:"modified_time" csv:"modified_time"`
	EmailAlertRecipients    []interface{} `json:"email_alert_recipients" csv:"email_alert_recipients"`
	SmsAlertRecipients      []interface{} `json:"sms_alert_recipients" csv:"sms_alert_recipients"`
	TwilioNumber            interface{}   `json:"twilio_number" csv:"twilio_number"`
	TwilioAccountSid        interface{}   `json:"twilio_account_sid" csv:"twilio_account_sid"`
	TwilioAuthToken         interface{}   `json:"twilio_auth_token" csv:"twilio_auth_token"`
	SmtpFromEmail           string        `json:"smtp_from_email" csv:"smtp_from_email"`
	SmtpHost                string        `json:"smtp_host" csv:"smtp_host"`
	SmtpHostUser            string        `json:"smtp_host_user" csv:"smtp_host_user"`
	SmtpHostPassword        string        `json:"smtp_host_password" csv:"smtp_host_password"`
	SmtpPort                int           `json:"smtp_port" csv:"smtp_port"`
	SmtpRequiresAuth        bool          `json:"smtp_requires_auth" csv:"smtp_requires_auth"`
	DefaultTimeZone         string        `json:"default_time_zone" csv:"default_time_zone"`
	CheckHistoryPruneDays   int           `json:"check_history_prune_days" csv:"check_history_prune_days"`
	ResolvedAlertsPruneDays int           `json:"resolved_alerts_prune_days" csv:"resolved_alerts_prune_days"`
	AgentHistoryPruneDays   int           `json:"agent_history_prune_days" csv:"agent_history_prune_days"`
	DebugLogPruneDays       int           `json:"debug_log_prune_days" csv:"debug_log_prune_days"`
	AuditLogPruneDays       int           `json:"audit_log_prune_days" csv:"audit_log_prune_days"`
	AgentDebugLevel         string        `json:"agent_debug_level" csv:"agent_debug_level"`
	ClearFaultsDays         int           `json:"clear_faults_days" csv:"clear_faults_days"`
	MeshToken               string        `json:"mesh_token" csv:"mesh_token"`
	MeshUsername            string        `json:"mesh_username" csv:"mesh_username"`
	MeshSite                string        `json:"mesh_site" csv:"mesh_site"`
	MeshDeviceGroup         string        `json:"mesh_device_group" csv:"mesh_device_group"`
	AgentAutoUpdate         bool          `json:"agent_auto_update" csv:"agent_auto_update"`
	WorkstationPolicy       int           `json:"workstation_policy" csv:"workstation_policy"`
	ServerPolicy            interface{}   `json:"server_policy" csv:"server_policy"`
	AlertTemplate           interface{}   `json:"alert_template" csv:"alert_template"`
}

type CoreVersion struct {
	Version string `json:"version" csv:"version"`
}

// GetCoreCodeSign will get the CoreCodeSign from the TRMM API.
func (t *TRMM) GetCoreCodeSign() *TRMM {
	if t.err != nil {
		return t
	}

	t.Core.CodeSign = t.setURL("core/codesign").
		apiGet(CoreCodeSign{}).
		response.Result().(*CoreCodeSign)
	return t
}

// GetCoreCustomFields will get the CoreCustomFieldsn from the TRMM API.
func (t *TRMM) GetCoreCustomFields() *TRMM {
	if t.err != nil {
		return t
	}

	t.Core.CustomFields = t.setURL("core/customfields").
		apiGet([]CoreCustomFields{}).
		response.Result().(*[]CoreCustomFields)
	return t
}

// GetCoreDashInfo will get the CoreDashInfo from the TRMM API.
func (t *TRMM) GetCoreDashInfo() *TRMM {
	if t.err != nil {
		return t
	}

	t.Core.DashInfo = t.setURL("core/dashinfo").
		apiGet(CoreDashInfo{}).
		response.Result().(*CoreDashInfo)
	return t
}

// GetCoreKeyStore will get the CoreKeyStore from the TRMM API.
func (t *TRMM) GetCoreKeyStore() *TRMM {
	if t.err != nil {
		return t
	}

	t.Core.KeyStore = t.setURL("core/keystore").
		apiGet([]CoreKeyStore{}).
		response.Result().(*[]CoreKeyStore)
	return t
}

// GetCoreSettings will get the CoreSettings from the TRMM API.
func (t *TRMM) GetCoreSettings() *TRMM {
	if t.err != nil {
		return t
	}

	t.Core.Settings = t.setURL("core/settings").
		apiGet(CoreSettings{}).
		response.Result().(*CoreSettings)
	return t
}

// GetCoreVersion will get the CoreVersion from the TRMM API.
func (t *TRMM) GetCoreVersion() *TRMM {
	if t.err != nil {
		return t
	}

	// core/version returns a single string.
	t.setURL("core/version").
		apiGet(nil)
	var v = CoreVersion{
		Version: strings.Trim(t.response.String(), "\""),
	}
	t.Core.Version = &v
	return t
}
