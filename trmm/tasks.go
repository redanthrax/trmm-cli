// Package trmm integrates with the Tactical RMM API.
package trmm

import "time"

// Tasks endpoint.
type Tasks struct {
	Id                     int            `json:"id"`
	Schedule               string         `json:"schedule"`
	LastRun                string         `json:"last_run"`
	AlertTemplate          interface{}    `json:"alert_template"`
	RunTimeDate            time.Time      `json:"run_time_date"`
	ExpireDate             interface{}    `json:"expire_date"`
	CreatedBy              string         `json:"created_by"`
	CreatedTime            string         `json:"created_time"`
	ModifiedBy             string         `json:"modified_by"`
	ModifiedTime           string         `json:"modified_time"`
	ScriptArgs             []interface{}  `json:"script_args"`
	Timeout                int            `json:"timeout"`
	Actions                []TasksActions `json:"actions"`
	Name                   string         `json:"name"`
	CollectorAllOutput     bool           `json:"collector_all_output"`
	ManagedByPolicy        bool           `json:"managed_by_policy"`
	ParentTask             interface{}    `json:"parent_task"`
	Retvalue               interface{}    `json:"retvalue"`
	Retcode                int            `json:"retcode"`
	Stdout                 string         `json:"stdout"`
	Stderr                 string         `json:"stderr"`
	ExecutionTime          string         `json:"execution_time"`
	Enabled                bool           `json:"enabled"`
	ContinueOnError        bool           `json:"continue_on_error"`
	Status                 string         `json:"status"`
	SyncStatus             string         `json:"sync_status"`
	AlertSeverity          string         `json:"alert_severity"`
	EmailAlert             bool           `json:"email_alert"`
	TextAlert              bool           `json:"text_alert"`
	DashboardAlert         bool           `json:"dashboard_alert"`
	TaskType               string         `json:"task_type"`
	WinTaskName            string         `json:"win_task_name"`
	DailyInterval          int            `json:"daily_interval"`
	RunTimeBitWeekdays     interface{}    `json:"run_time_bit_weekdays"`
	WeeklyInterval         int            `json:"weekly_interval"`
	RunTimeMinute          interface{}    `json:"run_time_minute"`
	MonthlyDaysOfMonth     interface{}    `json:"monthly_days_of_month"`
	MonthlyMonthsOfYear    interface{}    `json:"monthly_months_of_year"`
	MonthlyWeeksOfMonth    interface{}    `json:"monthly_weeks_of_month"`
	TaskRepetitionDuration interface{}    `json:"task_repetition_duration"`
	TaskRepetitionInterval interface{}    `json:"task_repetition_interval"`
	StopTaskAtDurationEnd  bool           `json:"stop_task_at_duration_end"`
	RandomTaskDelay        interface{}    `json:"random_task_delay"`
	RemoveIfNotScheduled   bool           `json:"remove_if_not_scheduled"`
	RunAsapAfterMissed     bool           `json:"run_asap_after_missed"`
	TaskInstancePolicy     int            `json:"task_instance_policy"`
	Agent                  int            `json:"agent"`
	Policy                 interface{}    `json:"policy"`
	CustomField            interface{}    `json:"custom_field"`
	Script                 interface{}    `json:"script"`
	AssignedCheck          *int           `json:"assigned_check"`
	CheckName              string         `json:"check_name,omitempty"`
}

type TasksActions struct {
	Name       string   `json:"name"`
	Type       string   `json:"type"`
	Script     int      `json:"script"`
	Timeout    int      `json:"timeout"`
	ScriptArgs []string `json:"script_args"`
}

// GetTasks will get the Tasks from the TRMM API.
func (t *TRMM) GetTasks() *TRMM {
	if t.err != nil {
		return t
	}

	t.Tasks = t.setURL("tasks").
		apiGet([]Tasks{}).
		response.Result().(*[]Tasks)
	return t
}
