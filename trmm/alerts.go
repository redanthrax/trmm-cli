// Package trmm integrates with the Tactical RMM API.
package trmm

// Alerts represents all Alerts endpoints.
type Alerts struct {
	Templates *[]AlertsTemplates
}

type AlertsTemplates struct {
	Id                          int           `json:"id"`
	AgentSettings               bool          `json:"agent_settings"`
	CheckSettings               bool          `json:"check_settings"`
	TaskSettings                bool          `json:"task_settings"`
	CoreSettings                []interface{} `json:"core_settings"`
	DefaultTemplate             bool          `json:"default_template"`
	AppliedCount                int           `json:"applied_count"`
	CreatedBy                   string        `json:"created_by"`
	CreatedTime                 string        `json:"created_time"`
	ModifiedBy                  string        `json:"modified_by"`
	ModifiedTime                string        `json:"modified_time"`
	Name                        string        `json:"name"`
	IsActive                    bool          `json:"is_active"`
	ActionArgs                  []interface{} `json:"action_args"`
	ActionTimeout               int           `json:"action_timeout"`
	ResolvedActionArgs          []interface{} `json:"resolved_action_args"`
	ResolvedActionTimeout       int           `json:"resolved_action_timeout"`
	EmailRecipients             []interface{} `json:"email_recipients"`
	TextRecipients              []interface{} `json:"text_recipients"`
	EmailFrom                   string        `json:"email_from"`
	AgentEmailOnResolved        bool          `json:"agent_email_on_resolved"`
	AgentTextOnResolved         bool          `json:"agent_text_on_resolved"`
	AgentAlwaysEmail            interface{}   `json:"agent_always_email"`
	AgentAlwaysText             interface{}   `json:"agent_always_text"`
	AgentAlwaysAlert            bool          `json:"agent_always_alert"`
	AgentPeriodicAlertDays      int           `json:"agent_periodic_alert_days"`
	AgentScriptActions          bool          `json:"agent_script_actions"`
	CheckEmailAlertSeverity     []interface{} `json:"check_email_alert_severity"`
	CheckTextAlertSeverity      []interface{} `json:"check_text_alert_severity"`
	CheckDashboardAlertSeverity []string      `json:"check_dashboard_alert_severity"`
	CheckEmailOnResolved        bool          `json:"check_email_on_resolved"`
	CheckTextOnResolved         bool          `json:"check_text_on_resolved"`
	CheckAlwaysEmail            interface{}   `json:"check_always_email"`
	CheckAlwaysText             interface{}   `json:"check_always_text"`
	CheckAlwaysAlert            bool          `json:"check_always_alert"`
	CheckPeriodicAlertDays      int           `json:"check_periodic_alert_days"`
	CheckScriptActions          bool          `json:"check_script_actions"`
	TaskEmailAlertSeverity      []interface{} `json:"task_email_alert_severity"`
	TaskTextAlertSeverity       []interface{} `json:"task_text_alert_severity"`
	TaskDashboardAlertSeverity  []string      `json:"task_dashboard_alert_severity"`
	TaskEmailOnResolved         bool          `json:"task_email_on_resolved"`
	TaskTextOnResolved          bool          `json:"task_text_on_resolved"`
	TaskAlwaysEmail             interface{}   `json:"task_always_email"`
	TaskAlwaysText              interface{}   `json:"task_always_text"`
	TaskAlwaysAlert             bool          `json:"task_always_alert"`
	TaskPeriodicAlertDays       int           `json:"task_periodic_alert_days"`
	TaskScriptActions           bool          `json:"task_script_actions"`
	ExcludeWorkstations         bool          `json:"exclude_workstations"`
	ExcludeServers              bool          `json:"exclude_servers"`
	Action                      interface{}   `json:"action"`
	ResolvedAction              interface{}   `json:"resolved_action"`
	ExcludedSites               []interface{} `json:"excluded_sites"`
	ExcludedClients             []interface{} `json:"excluded_clients"`
	ExcludedAgents              []interface{} `json:"excluded_agents"`
}

// GetAlertsTemplates will get the AlertsTemplates from the TRMM API.
func (t *TRMM) GetAlertsTemplates() *TRMM {
	if t.err != nil {
		return t
	}

	t.Alerts.Templates = t.setURL("alerts/templates").
		apiGet([]AlertsTemplates{}).
		response.Result().(*[]AlertsTemplates)
	return t
}
