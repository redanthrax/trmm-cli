// Package trmm integrates with the Tactical RMM API.
package trmm

type Clients struct {
	Id                     int                       `json:"id" csv:"id"`
	Name                   string                    `json:"name" csv:"name"`
	ServerPolicy           interface{}               `json:"server_policy" csv:"server_policy"`
	WorkstationPolicy      interface{}               `json:"workstation_policy" csv:"workstation_policy"`
	AlertTemplate          interface{}               `json:"alert_template" csv:"alert_template"`
	BlockPolicyInheritance bool                      `json:"block_policy_inheritance" csv:"block_policy_inheritance"`
	Sites                  []ClientsSites            `json:"sites" csv:"sites"`
	CustomFields           []interface{}             `json:"custom_fields" csv:"custom_fields"`
	AgentCount             int                       `json:"agent_count" csv:"agent_count"`
	MaintenanceMode        bool                      `json:"maintenance_mode" csv:"maintenance_mode"`
	FailingChecks          ClientsSitesFailingChecks `json:"failing_checks" csv:"failing_checks"`
}

type ClientsSites struct {
	Id                     int                       `json:"id" csv:"id"`
	Name                   string                    `json:"name" csv:"name"`
	ServerPolicy           interface{}               `json:"server_policy" csv:"server_policy"`
	WorkstationPolicy      interface{}               `json:"workstation_policy" csv:"workstation_policy"`
	AlertTemplate          interface{}               `json:"alert_template" csv:"alert_template"`
	ClientName             string                    `json:"client_name" csv:"client_name"`
	Client                 int                       `json:"client" csv:"client"`
	CustomFields           []interface{}             `json:"custom_fields" csv:"custom_fields"`
	AgentCount             int                       `json:"agent_count" csv:"agent_count"`
	BlockPolicyInheritance bool                      `json:"block_policy_inheritance" csv:"block_policy_inheritance"`
	MaintenanceMode        bool                      `json:"maintenance_mode" csv:"maintenance_mode"`
	FailingChecks          ClientsSitesFailingChecks `json:"failing_checks" csv:"failing_checks"`
}

type ClientsSitesFailingChecks struct {
	Error   bool `json:"error"`
	Warning bool `json:"warning"`
}

// GetClients will get the Clients from the TRMM API.
func (t *TRMM) GetClients() *TRMM {
	if t.err != nil {
		return t
	}

	t.Clients = t.setURL("clients").
		apiGet([]Clients{}).
		response.Result().(*[]Clients)
	return t
}
