# OpenAPI 3

Django supports OpenAPI 3 and is exposed by enabling swagger. [oapi-codegen](https://github.com/deepmap/oapi-codegen)
can generate Go code, but since the schema does not expose the field names, the generated code does not have structs
with fields.

## Generation

This uses [xh](https://github.com/ducaale/xh), a Rust alternative to [HTTPie](https://github.com/httpie/httpie).

```bash
xh --json "https://api.example.com/api/schema/" > trmm-schema.json
xh --json "https://api.example.com/api/schema/?format=yaml" > trmm-schema.yaml

go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest

oapi-codegen --config server.cfg.yaml trmm-schema.yaml
oapi-codegen --config types.cfg.yaml trmm-schema.yaml
oapi-codegen --config clients.cfg.yaml trmm-schema.yaml
```
